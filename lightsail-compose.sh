#!/bin/bash
# This script is derived from this video: https://www.youtube.com/watch?v=z525kfneC6E

# install latest version of docker the lazy way
curl -sSL https://get.docker.com | sh

# make it so you don't need to sudo to run docker commands
usermod -aG docker ubuntu

# install docker-compose
curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# copy the dockerfile into /srv/docker 
# if you change this, change the systemd service file to match
# WorkingDirectory=[whatever you have below]
mkdir /srv/devmaker
curl -o /srv/devmaker/docker-compose.yml https://bitbucket.org/luispspereira/devmaker-website/raw/45da068ea206c3a5acbbe9a7249d3aa98c1a16d5/docker-compose.yml

# copy in systemd unit file and register it so our compose file runs 
# on system restart
curl -o /etc/systemd/system/docker-compose-app.service https://bitbucket.org/luispspereira/devmaker-website/raw/9758b09fd336cddef233dc2744f3a16223e93de6/docker-compose-app.service
systemctl enable docker-compose-app

# start up the application via docker-compose
docker-compose -f /srv/devmaker/docker-compose.yml up -d 