# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end

# Layouts
# https://middlemanapp.com/basics/layouts/
#
# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false
page '/calc_orcamento.html', layout: false
page '/*_og.html', layout: false

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Set slim-lang output style
Slim::Engine.set_options :pretty => true

# Set template languages
set :slim, :layout_engine => :slim

activate :sprockets

# Add image-optim
# activate :imageoptim

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

page "portfolio/*", :layout => "subportfolio.html"

helpers do
  def render_metatags(current_path)
    partials_metatags = {
      'index.html' => "index_mt",
      'orcamento.html' => "orcamento_mt",
      'contato.html' => "contato_mt",
      'clientes.html' => "clientes_mt",
      'empresa.html' => "empresa_mt",
      'portfolio.html' => "portfolio_mt"
    }
    partial("partials/metatags/" + partials_metatags[current_path])
  end

  def make_body(current_path)
    if current_path == 'index.html'
      partial("partials/body", :locals => { :home => "home", :page_id => "551" })
    else
      partials_ids = {
        'orcamento.html' => "3659",
        'contato.html' => "26",
        'clientes.html' => "3438",
        'empresa.html' => "704",
        'portfolio.html' => "743"
      }
      partial("partials/body", :locals => { :home => "", :page_id => partials_ids[current_path] })
    end
  end

  def subportfolio_metadata (current_path)
    metadata = {
      'portfolio/abert/index.html'                  => { :app_name => "ABERT", :app_image => "abert_large.png", :app_link => "http://devmaker.com.br/portfolio/abert/" },
      'portfolio/aulas-de-direito/index.html'       => { :app_name => "Aulas de Direito", :app_image => "bittencourt_large.png", :app_link => "http://devmaker.com.br/portfolio/aulas-de-direito/" },
      'portfolio/be-a-ba-da-eletrica/index.html'    => { :app_name => "BE-A-BÁ da Elétrica", :app_image => "beaba_large.png", :app_link => "http://devmaker.com.br/portfolio/be-a-ba-da-eletrica/" },
      'portfolio/broker-contact/index.html'         => { :app_name => "Broker Contact", :app_image => "broker_large.png", :app_link => "http://devmaker.com.br/portfolio/broker-contact/" },
      'portfolio/cembra/index.html'                 => { :app_name => "CEMBRA", :app_image => "cembra_large.png", :app_link => "http://devmaker.com.br/portfolio/cembra/" },
      'portfolio/chaves-na-mao/index.html'          => { :app_name => "Chaves na Mão", :app_image => "image_big.jpg", :app_link => "http://devmaker.com.br/portfolio/chaves-na-mao/" },
      'portfolio/desentupidor-de-ideias/index.html' => { :app_name => "Desentupidor de Ideias", :app_image => "desentupidor.jpg", :app_link => "http://devmaker.com.br/portfolio/desentupidor-de-ideias/" },
      'portfolio/duelo-de-make/index.html'          => { :app_name => "Duelo de Make", :app_image => "duelodemake_large.png", :app_link => "http://devmaker.com.br/portfolio/duelo-de-make/" },
      'portfolio/euroit/index.html'                 => { :app_name => "Euroit", :app_image => "euroit_large.png", :app_link => "http://devmaker.com.br/portfolio/euroit/" },
      'portfolio/footsys/index.html'                => { :app_name => "FootSys", :app_image => "footsys_large.png", :app_link => "http://devmaker.com.br/portfolio/footsys/" },
      'portfolio/imobmaker/index.html'              => { :app_name => "ImobMaker", :app_image => "imobmaker_large.jpg", :app_link => "http://devmaker.com.br/portfolio/imobmaker/" },
      'portfolio/jba-imoveis/index.html'            => { :app_name => "JBA Imóveis", :app_image => "jba.jpg", :app_link => "http://devmaker.com.br/portfolio/jba-imoveis/" },
      'portfolio/placo/index.html'                  => { :app_name => "Placo Forros Acústico", :app_image => "placo_large.png", :app_link => "http://devmaker.com.br/portfolio/placo/" },
      'portfolio/placoteca/index.html'              => { :app_name => "Placoteca", :app_image => "placoteca_large.png", :app_link => "http://devmaker.com.br/portfolio/placoteca/" },
      'portfolio/pocket-you/index.html'             => { :app_name => "PokeYou", :app_image => "pokeyou_large.png", :app_link => "http://devmaker.com.br/portfolio/pocket-you/" },
      'portfolio/radio-controle/index.html'         => { :app_name => "Rádio Controle", :app_image => "band.jpg", :app_link => "http://devmaker.com.br/portfolio/radio-controle/" },
      'portfolio/so-carrao/index.html'              => { :app_name => "SóCarrão", :app_image => "so_carrao_large.png", :app_link => "http://devmaker.com.br/portfolio/so-carrao/" },
      'portfolio/tim/index.html'                    => { :app_name => "TIM Pesquisa", :app_image => "tim_large.png", :app_link => "http://devmaker.com.br/portfolio/tim/" },
      'portfolio/uningresso/index.html'             => { :app_name => "UNIngresso", :app_image => "big_uni-2.jpg", :app_link => "http://devmaker.com.br/portfolio/uningresso/" },
      'portfolio/univirtus/index.html'              => { :app_name => "Univirtus", :app_image => "big_uninter-2.jpg", :app_link => "http://devmaker.com.br/portfolio/univirtus/" }
    }

    partial("partials/subportfolio_meta", :locals => metadata[current_path])
  end
end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

configure :build do
  activate :minify_css
  activate :minify_javascript
end

after_build do |builder|
	system("bash deploy_static.sh") 
	#system("bash deploy.sh -b") 
end
