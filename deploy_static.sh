#!/bin/bash

echo "Compressing archives..."
tar -cjf build.tar.bz2 build

echo "Sending archives to server..."
if [[ ("$1" = "-c") || ("$1" = "--config") ]]; then
	scp -i lightsail_default.pem nginx.conf ubuntu@3.82.99.39:~/.
fi
scp -i lightsail_default.pem -r build.tar.bz2 ubuntu@3.82.99.39:~/.

echo "Cleaning local directory..."
rm build.tar.bz2

# if [[ ("$1" = "-c") || ("$1" = "--config") ]]; then
#    	echo "Sending nginx configutarion and restarting server..."

# ssh -i lightsail_default.pem ubuntu@3.82.99.39 << EOF
# 	cd /srv/devmaker/

# 	rm nginx.conf
# 	sudo mv ~/nginx.conf .

# 	docker-compose restart
# EOF

# fi

ssh -i lightsail_default.pem ubuntu@3.82.99.39 << EOF
	rm -r build

	echo "Extracting static site..."
	tar xfj build.tar.bz2	
	rm build.tar.bz2

	cd /srv/devmaker/

	echo "Cleaning and Copying build to the right directory..."
	sudo cp -r ~/build .
	rm -r ~/build
EOF

	# SID=`docker ps -qf "name=devmaker"`
	# docker exec $SID nginx -s reload

echo "End of deployment script."

