#!/bin/bash


if [[ ("$1" = "-b") || ("$1" = "--build") ]]; then
	docker build -t devmaker_web .
fi

docker save devmaker_web | bzip2 | ssh -i lightsail_default.pem ubuntu@3.82.99.39 'bunzip2 | docker load'

ssh -i lightsail_default.pem ubuntu@3.82.99.39 << EOF
	cd /srv/devmaker/
	docker-compose restart
	docker-compose -f /srv/devmaker/docker-compose.yml up -d
EOF
