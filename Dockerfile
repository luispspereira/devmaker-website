# Copy the resulting code to the nginx container
FROM nginx:alpine
COPY /build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
